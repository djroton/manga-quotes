import React, { useEffect, useState } from 'react';
import { Text, View, StatusBar, SafeAreaView, ScrollView, StyleSheet, Pressable } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

function App() {
  const [isLoading, setLoading] = useState(true);
  const [quote, setQuote] = useState([]);
  const [languages, setLanguages] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState(false);
  const [translatedQuote, setTranslatedQuote] = useState(0);

  function getLanguage() {
    fetch('https://libretranslate.de/languages')
      .then((response) => response.json())
      .then((json) => setLanguages(json))
      .catch((error) => console.error(error))
  }

  function fetchQuote() {
    fetch('https://api.rei.my.id/v2/quotes')
      .then((response) => response.json())
      .then((json) => setQuote(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }

  function translate() {
    fetch("https://libretranslate.de/translate", {
      body: `q=${quote.quote}&source=en&target=${selectedLanguage}&format=text`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: "POST"
    })
      .then((response) => response.json())
      .then((json) => setTranslatedQuote(json))
      .catch((error) => console.error(error))
  }

  function reload() {
    fetch('https://api.rei.my.id/v2/quotes')
      .then((response) => response.json())
      .then((json) => {
        setQuote(json), fetch("https://libretranslate.de/translate", {
          body: `q=${json.quote}&source=en&target=${selectedLanguage}&format=text`,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
          },
          method: "POST"
        })
          .then((response) => response.json())
          .then((json) => setTranslatedQuote(json))
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }

  useEffect(() => {
    getLanguage()
    fetchQuote()
  }, []);


  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={{
          marginHorizontal: 10,
          marginTop: 15
        }}>
          <StatusBar backgroundColor={'darkred'} />
          <Text style={{
            fontFamily: 'Roboto',
            fontSize: 30,
            fontWeight: 'bold',
            color: "white",
            textAlign: 'center'
          }}>Manga Quotes
          </Text>
        </View>
        <View>
          {isLoading ? <Text>Loading...</Text> :
            (<View >
              <View style={{
                backgroundColor: 'black',
                marginHorizontal: 10,
                borderRadius: 10,
                marginTop: 20,
                paddingHorizontal: 15,
                paddingVertical: 20
              }}>
                <Text style={{
                  color: 'darkred',
                  fontWeight: 'bold',
                  fontSize: 22
                }}><MaterialCommunityIcons name="movie" size={24} color="yellow" /> {quote.anime}</Text>
                <Text style={{
                  color: 'darkred',
                  fontWeight: 'bold',
                  fontSize: 17,
                }}><MaterialCommunityIcons name="horse-human" size={24} color="yellow" />  {quote.name} </Text>
                <Text style={{
                  color: 'white',
                  fontSize: 15,
                  textAlign: 'justify',
                  borderColor: 'darkred',
                  borderRadius: 5,
                  padding: 10,
                  borderWidth: 2,
                }}><MaterialCommunityIcons name="comment-quote" size={24} color="yellow" /> {quote.quote} </Text>
              </View>
              <View style={{
                marginHorizontal: 10,
                marginTop: 20,
              }}>
                {!languages ? <Text> </Text> : (
                  <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedLanguage(itemValue)
                    }
                    style={{
                      backgroundColor: 'yellow'
                    }}>
                    {languages.length > 0 ? languages.map(item => <Picker.Item label={item.name} value={item.code} />) : ''}
                  </Picker>)}
              </View>
              <View style={{
                marginHorizontal: 10,
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>


                <Pressable style={{
                  paddingHorizontal: 20
                }} onPress={translate}>
                  <MaterialCommunityIcons name="translate" size={30} color="yellow" />
                </Pressable>

                <Pressable style={{
                  paddingHorizontal: 20
                }} onPress={reload}>
                  <MaterialCommunityIcons name="reload" size={30} color="yellow" />
                </Pressable>

              </View>
              {!translatedQuote ? <Text> </Text> : (
                <View style={{
                  backgroundColor: 'black',
                  marginHorizontal: 10,
                  borderRadius: 10,
                  marginTop: 20,
                  paddingHorizontal: 15,
                  paddingVertical: 20
                }}>
                  <Text style={{
                    color: 'white',
                    fontSize: 15,
                    textAlign: 'justify',
                    borderColor: 'darkred',
                    borderRadius: 5,
                    padding: 10,
                    borderWidth: 2,
                  }}> <MaterialCommunityIcons name="comment-quote" size={24} color="yellow" /> {translatedQuote.translatedText} </Text>
                </View>
              )}
            </View>
            )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'darkred'
  }
})
export default App;